package clases;

import java.io.Serializable;

public class Coches implements Serializable {

	/**
	 * Creacion de la clase coches y atributos
	 */
	private static final long serialVersionUID = 1L;
	private String matricula;
	private String marca;
	private String modelo;
	private int a�o;
	private float kilometros;
	private String color;
	
	/**
	 * Constructor 
	 * @param matricula
	 * @param marca
	 */
	public Coches(String matricula, String marca) {
		super();
		this.matricula = matricula;
		this.marca = marca;
	}

	/**
	 * Setter y getter
	 * @return
	 */
	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getA�o() {
		return a�o;
	}

	public void setA�o(int a�o) {
		this.a�o = a�o;
	}

	public float getKilometros() {
		return kilometros;
	}

	public void setKilometros(float kilometros) {
		this.kilometros = kilometros;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	/**
	 * ToString coche
	 */
	@Override
	public String toString() {
		return "Coches [matricula=" + matricula + ", marca=" + marca + ", modelo=" + modelo + ", a�o=" + a�o
				+ ", kilometros=" + kilometros + ", color=" + color + "]";
	}

	
	
	
}
