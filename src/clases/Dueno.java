package clases;

import java.io.Serializable;

public class Dueno implements Serializable{
	
	/**
	 * Creacion clase due�o y atributos
	 */
	private static final long serialVersionUID = 1L;
	private String nombre;
	private String apellido;
	private int dni;
	private int edad;
	private String carnet;
	private Coches coche;
	
	/**
	 * Dueno constructor
	 * @param nombre
	 * @param dni
	 * @param coche
	 */
	public Dueno(String nombre, int dni, Coches coche){
		super();
		this.nombre = nombre;
		this.dni = dni;
		this.coche = coche;
	}

	/**
	 * Setter y getter due�o
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public int getDni() {
		return dni;
	}

	public void setDni(int dni) {
		this.dni = dni;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getCarnet() {
		return carnet;
	}

	public void setCarnet(String carnet) {
		this.carnet = carnet;
	}
	
	public Coches getCoche() {
		return coche;
	}

	public void setCoche(Coches coche) {
		this.coche = coche;
	}

	/**
	 * ToString
	 */
	@Override
	public String toString() {
		return "Dueno [nombre=" + nombre + ", apellido=" + apellido + ", dni=" + dni + ", edad=" + edad + ", carnet="
				+ carnet + ", coche=" + coche + ", toString()=" + super.toString() + "]";
	}

	
	
	
}
