package clases;

import java.io.BufferedReader;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Scanner;

public class FicheroAccesoAleatorio {
	/**
	 * Creamos unos atributos para el ficheroaletatorio
	 */
	private ArrayList <Coches> coches;
	private String archivo;

	/**
	 * Constructor 
	 * @param archivo
	 */
	public FicheroAccesoAleatorio(String archivo) {
		coches = new ArrayList<Coches>();
		this.archivo = archivo;
	}
	/**
	 * Metodo para rellenar el fichero
	 */
	public void rellenarArchivo() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			f.seek(f.length());
			String respuesta = "";
			do {
				System.out.println("Nombre ");
				String nombre = in.readLine();
				nombre = formatearNombre(nombre, 20);
				f.writeUTF(nombre);
				System.out.println("�Deseas continuar (si/no)?");
				respuesta = in.readLine();
			} while (respuesta.equalsIgnoreCase("si"));
			f.close();
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	
	/**
	 * Formatearemos el nombre del fichero
	 * @param nombre
	 * @param lon
	 * @return
	 */
	private String formatearNombre(String nombre, int lon) {
		if (nombre.length() > lon) {
			return nombre.substring(0, lon);
		} else {
			for (int i = nombre.length(); i < lon; i++) {
				nombre = nombre + " ";
			}
		}
		return nombre;
	}

	/**
	 * Visualizar los productos del fichero
	 */
	public void visualizarProducto() {
		for (Coches coches2 : coches) {
			System.out.println("Matricula --> " + coches2.getMatricula());
			System.out.println("Marca --> " + coches2.getMarca());
		}
	}
	
	/**
	 * Visualizar el archivo entero/fichero
	 */
	public void visualizarArchivo() {
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			boolean finFichero = false;
			do {
				try {
					nombre = f.readUTF();
					System.out.println(nombre);
				} catch (EOFException e) {
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();
				}

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	
	/**
	 * Aqui buscamos el dato que necesitemos en el fichero
	 */
	public void buscarEnElArchivo() {
		Scanner input = new Scanner(System.in);
		try {
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre = "";
			System.out.println("Escribe el nombre a buscar");
			String nombreBuscar = input.nextLine();
			boolean finFichero = false;
			do {
					nombre = f.readUTF();
					if (nombre == nombreBuscar) {
						System.out.println(nombre);
					} else {
						System.out.println("Palabra no encontrada");
					}
				
					System.out.println("Fin fichero");
					finFichero = true;
					f.close();

			} while (!finFichero);
		} catch (IOException e) {
			System.out.println("error");
		}
		input.close();
	}
	
	/**
	 * En este metodo modificamos el archivo
	 */
	public void modificarArchivo() {
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
			String oldName = "Elena";
			String newName = "Elena Jim�nez";
			System.out.println("Nombre a modificar");
			oldName = in.readLine();
			System.out.println("Dame el nuevo nombre");
			newName = in.readLine();
			RandomAccessFile f = new RandomAccessFile(archivo, "rw");
			String nombre;
			boolean finFichero = false;
			boolean valorEncontrado = false;
			do {
				try {
					nombre = f.readUTF();
					if (nombre.trim().equalsIgnoreCase(oldName)) {
						f.seek(f.getFilePointer() - 22);
						newName = formatearNombre(newName, 20);
						f.writeUTF(newName);
						valorEncontrado = true;
					}
				} catch (EOFException e) {
					f.close();
					finFichero = true;
				}
			} while (!finFichero);
			if (valorEncontrado == false) {
				System.out.println("El valor no est� en el fichero");
			} else {
				System.out.println("El valor ha sido modificado");
			}
		} catch (IOException e) {
			System.out.println("error");
		}
	}
	
}
