package clases;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.util.ArrayList;

public class FicheroSecuencial implements Serializable{

	/**
	 * Fichero secuencial, atributos
	 */
	private static final long serialVersionUID = 1L;
	private String archivo;
	private ArrayList<Coches> listaCoches;
	private ArrayList<Dueno> listaDuenos;

	/**
	 * Constructor
	 * @param archivo
	 */
	public FicheroSecuencial(String archivo) {
		this.archivo = archivo;
		listaCoches = new ArrayList<Coches>();
		listaDuenos = new ArrayList<Dueno>();
	}
	
	/**
	 * Creamos el archivo
	 * @throws IOException
	 */
	public void crearArchivo() throws IOException{
		System.out.println("Creo el archivo secuencial");
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String linea;
		PrintWriter fuente = new PrintWriter(new BufferedWriter(new FileWriter(this.archivo)));
		System.out.println("Introduce lineas (para acabar *)");
		linea = in.readLine();
		while (!linea.equalsIgnoreCase("*")) {
			fuente.println(linea);
			linea = in.readLine();
		}
		System.out.println("archivo creado");
		fuente.close();
	}
	
	/**
	 * Visualizamos el archivo creado
	 * @throws IOException
	 */
	public void visualizarArchivo() throws IOException {
		String linea;
		System.out.println("VISUALIZAR ARCHIVO " + this.archivo);
		BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
		linea = fuente.readLine();
		while (linea != null) {
			System.out.println(linea);
			linea = fuente.readLine();
		}
		fuente.close();
	}
	
	/**
	 * Guardaremos los datos en el archivo
	 */
	public void guardarDatos() {
		try {
			ObjectOutputStream escritor = new ObjectOutputStream(new FileOutputStream(new File("src/datosSecuenciales.dat")));
			escritor.writeObject("datosSecuenciales.dat");
			escritor.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Cargaremos los datos para visualizarlos
	 */
	@SuppressWarnings("unchecked")
	public void cargarDatos() {
		try {
			ObjectInputStream escritor = new ObjectInputStream(new FileInputStream(new File("src/datosSecuenciales.dat")));
			listaCoches = (ArrayList<Coches>) escritor.readObject();
			escritor.close();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Alta del coche
	 * @param matricula
	 * @param marca
	 */
	public void altaCoche(String matricula, String marca) {
		listaCoches.add(new Coches(matricula, marca));
		guardarDatos();
	}
	
	/**
	 * Alta del dueño
	 * @param nombre
	 * @param dni
	 * @param matricula
	 */
	public void altaDueno(String nombre, int dni, String matricula) {
		listaDuenos.add(new Dueno(nombre, dni, devuelveCoche(matricula)));
		guardarDatos();
	}
	
	/**
	 * Asiganremos un coche al dueño elegido
	 * @param nombre
	 * @param dni
	 * @param matricula
	 */
	public void asignarCocheDueno(String nombre, int dni, String matricula) {
		if (duenoExiste(dni)) {
			if (cocheExiste(matricula)) {
				listaDuenos.add(new Dueno(nombre, dni,  devuelveCoche(matricula)));
			}
		}
		guardarDatos();
	}
	
	/**
	 * Comrpobar si el coche existen
	 * @param matricula
	 * @return
	 */
	public boolean cocheExiste(String matricula) {
		for (Coches coche:listaCoches) {
			if (coche.getMatricula().equalsIgnoreCase(matricula)) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Comprobar el dueño, si existe
	 * @param dni
	 * @return
	 */
	public boolean duenoExiste(int dni) {
		for (Dueno dueno: listaDuenos) {
			if (dueno.getDni()==dni) {
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Nos devolvera un coche segun la matricula
	 * @param matricula
	 * @return
	 */
	public Coches devuelveCoche(String matricula) {
		for (Coches coche : listaCoches) {
			if (coche.getMatricula()==matricula) {
				return coche;
			}
		}
		return null;
	}
	
	/**
	 * Devuelve un dueño por dni
	 * @param dni
	 * @return
	 */
	public Dueno devuelveDueno(int dni) {
		for (Dueno dueno : listaDuenos) {
			if (dueno.getDni()==dni) {
				return dueno;
			}
		}
		return null;
	}
	
	/**
	 * Lista de coches
	 */
	public void listarCoches() {
		for (Coches coches : listaCoches) {
			System.out.println(coches);
		}
		guardarDatos();
	}
	
	/**
	 * Lista de dueños
	 */
	public void listarDuenos() {
		for (Dueno dueno : listaDuenos) {
			System.out.println(dueno);
		}
		guardarDatos();
	}
	
	/**
	 * Buscamos un coche por su matricula
	 * @param matricula2
	 * @throws IOException
	 */
	public void buscarCoche(String matricula2) throws IOException {
			for (Coches coches : listaCoches) {
				if (cocheExiste(matricula2)){
					System.out.println(coches);	
				} else {
					System.out.println("No existe ese coche");
				}
			}
	}
	
	/**
	 * Buscar dueño por su dni
	 * @param dni2
	 * @throws IOException
	 */
	public void buscarDueno(int dni2) throws IOException {
		for (Dueno dueno : listaDuenos) {
			if (duenoExiste(dni2)){
				System.out.println(dueno);	
			} else {
				System.out.println("No existe ese dueño");
			}
		}
	}
	
	/**
	 * Contaremos el numero de lineas del archivo
	 * @return
	 */
	public int numeroLineas() {
		System.out.println("CONTAR LINEAS");
		String linea;
		int contadorLineas = 0;
		try {
			BufferedReader fuente = new BufferedReader(new FileReader(this.archivo));
			
			linea = fuente.readLine();
			while (linea != null) {
				contadorLineas++;
				linea = fuente.readLine();
			}
			
						fuente.close();
					} catch (FileNotFoundException e) {
						System.out.println("Fichero no encontrado");
						System.exit(0);
					} catch (IOException e) {
						System.out.println("Error de entrada salida");
						System.exit(0);
					}
					return contadorLineas;

				}
}
