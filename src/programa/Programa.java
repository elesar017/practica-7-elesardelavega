package programa;

import java.io.IOException;
import java.util.Scanner;

import clases.FicheroAccesoAleatorio;
import clases.FicheroSecuencial;

public class Programa {
	static public Scanner in = new Scanner(System.in);
	public static void main(String[] args) {
		
		/**
		 * Creamos los ficheros
		 */
		int opcion1;
		FicheroSecuencial miArchivo= new FicheroSecuencial("datosSecuenciales");
		FicheroAccesoAleatorio archivoAleatorio = new FicheroAccesoAleatorio("datosAleatorios");
		
		try {
			miArchivo.crearArchivo();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Practica 7 ElesarDeLaVega");

		/**
		 * Visualizamos el menu que se repetira hasta que salgamos con el 3
		 */
		do {
			System.out.println("En que fichero vamos a trabajar");
			System.out.println("1 - Fichero secuencial");
			System.out.println("2 - Fichero aleatorio");
			System.out.println("3 - Ninguno/Salir");
			opcion1 = in.nextInt();
			switch(opcion1) {
			case 1:
				/**
				 * Entramos al fichero secuencial y con el menu elegiremos los metodos creados
				 */
				int opcionSecuencial;
				System.out.println("FICHERO SECUENCIAL");
				System.out.println("Creamos o entramos al fichero secuencial datos");
				
				
				
				System.out.println("Elige que quieres hacer");
				System.out.println("1 - Dar de alta un coche");
				System.out.println("2 - Dar de alta un due�o");
				System.out.println("3 - Buscar un coche");
				System.out.println("4 - Buscar un due�o");
				System.out.println("5 - Cantidad de lineas del fichero");
				System.out.println("6 - Visualizar archivo");
				System.out.println("7 - Salir");
				opcionSecuencial = in.nextInt();
				
				switch(opcionSecuencial) {
				case 1:
					in.nextLine();
					System.out.println("Escribe la matricula del coche");
					String matricula = in.nextLine();
					System.out.println("Escribe la marca del coche");
					String marca = in.nextLine();
					miArchivo.altaCoche(matricula, marca);
					miArchivo.listarCoches();
					
					break;
				case 2:
					
					in.nextLine();
					System.out.println("Escribe el nombre del due�o");
					String nombre = in.nextLine();
					System.out.println("Escribe el dni del due�o");
					int dni = in.nextInt();
					System.out.println("Escribe la matricula del coche que tiene");
					in.nextLine();
					String matricula1 = in.nextLine();
					miArchivo.altaDueno(nombre, dni, matricula1);
					miArchivo.asignarCocheDueno(nombre, dni, matricula1);
					miArchivo.listarDuenos();
					break;
				case 3:
					in.nextLine();
					System.out.println("Escribe la matricula del coche");
					String matricula2 = in.nextLine();
					try {
						miArchivo.buscarCoche(matricula2);
					} catch (IOException e1) {
						
						e1.printStackTrace();
					}
					break;
				case 4:
					in.nextLine();
					System.out.println("Escribe el dni del due�o");
					int dni2 = in.nextInt();
					try {
						miArchivo.buscarDueno(dni2);
					} catch (IOException e1) {
						
						e1.printStackTrace();
					}
					break;
				case 5:
					in.nextLine();
					miArchivo.numeroLineas();
					break;
				case 6:
					try {
						miArchivo.visualizarArchivo();
					} catch (IOException e) {
						e.printStackTrace();
					}
					break;
				case 7:
					System.exit(0);
					break;
				default: System.out.println("Debes elegir una opcion valida.");
				}
				break;
					
			case 2:
				/**
				 * Segunda opcion, fichero aleatorio donde seleccionamos que hacer con el menu
				 */
				int opcionAleatoria;
				System.out.println("FICHERO ALEATORIO");
				System.out.println("Creamos o entramos al fichero aleatorio datos2");
				
				System.out.println("Elige que quieres hacer");
				System.out.println("1 - Rellenar archivo");
				System.out.println("2 - Buscar en el archivo");
				System.out.println("3 - Modificar fichero");
				System.out.println("4 - Visualizar fichero");
				System.out.println("5 - Visualizar producto");
				System.out.println("6 - Salir");
				opcionAleatoria = in.nextInt();
				
				switch(opcionAleatoria) {
				case 1:
					in.nextLine();
					archivoAleatorio.rellenarArchivo();
					break;
				case 2:
					archivoAleatorio.buscarEnElArchivo();
					break;
				case 3:
					archivoAleatorio.modificarArchivo();
					break;
				case 4:
					archivoAleatorio.visualizarArchivo();
					break;
				case 5:
					archivoAleatorio.visualizarProducto();
					break;
				case 6:
					
					break;
				default: System.out.println("Opcion no valida");
				}
				break;
			case 3 :
				/**
				 * Salir, cerrar programa
				 */
				System.out.println("Vuelve pronto, adios.");
				System.exit(0);
				break;
			default: System.out.println("Debes elegir una opcion valida");
			}
		} while (opcion1 != 3);
		in.close();
	}


}
